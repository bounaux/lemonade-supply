package com.ciber.beveragevendor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;

/***
 * Controller returns a serving of lemonade as in JSON
 *
 */
@Slf4j
@RestController
@RequestMapping("/lemonade")
public class LemonadeController {

    private final Counter lemonadeServings;
    private final Counter lemonadeCost;

    public LemonadeController(MeterRegistry registry) {
        this.lemonadeServings = registry.counter(this.getClass().getPackage().getName() + ".lemonade.servings");
        this.lemonadeCost = registry.counter(this.getClass().getPackage().getName() + ".lemonade.cost");
    }

    @GetMapping("/{amount}")
    public BeverageServing getServing(@PathVariable Integer amount) {
        BeverageServing serving = new BeverageServing("Lemonade", amount);
        log.info("LemonadeServing {}", serving);

        lemonadeServings.increment();
        lemonadeCost.increment(serving.getCostCents());

        return serving;
    }

}
